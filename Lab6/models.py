from django.db import models
from django.utils import timezone

class Status(models.Model):
	status = models.TextField(max_length = 200)
	date = models.DateTimeField(auto_now_add = True)


# Create your models here.
