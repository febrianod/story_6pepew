from django.shortcuts import render
from django.shortcuts import redirect
from django.http import HttpResponseRedirect
from .forms import Status_Form
from .models import Status

# Create your views here.
response = {}

def index(request):
	mdlstatus = Status.objects.all()
	frmstatus = Status_Form(request.POST)
	if request.method == 'POST':
		response['status'] = request.POST['status']
		stat = Status(status = response['status'])
		stat.save()
		form = Status_Form()
		context = {
			'frmstatus' : frmstatus,
			'mdlstatus' : mdlstatus
		}
		return render(request, "Status.html", context)

	else :
		form = Status_Form()
		context = {
			'frmstatus' : frmstatus,
			'mdlstatus' : mdlstatus
		}
		return render(request, "Status.html", context)

# Create your views here.
