from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import index
from .models import Status
from django.utils import timezone
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest


class FunctionalTest(LiveServerTestCase):
	def setUp(self):
		super(FunctionalTest,self).setUp()
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		service_log_path = "./chromedriver.log"
		service_args = ['--verbose']
		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		self.selenium.implicitly_wait(25) 
		'''
		self.selenium  = webdriver.Chrome()
		'''

	def tearDown(self):
		self.selenium.quit()

	def test_input_status(self):
		self.selenium.get('%s%s' % (self.live_server_url, '/Lab7/'))
		status = self.selenium.find_element_by_id('id_status')
		submit = self.selenium.find_element_by_id('submit_button')
		status.send_keys('coba-coba')
		submit.click()
		self.assertIn("coba-coba", self.selenium.page_source)

	def test_layout_jumbotron(self):
		self.selenium.get('%s%s' %(self.live_server_url, '/Lab7/'))
		self.assertIn('Hai, Apa Kabar?', self.selenium.page_source)

	def test_layout_status(self):
		self.selenium.get('%s%s' %(self.live_server_url, '/Lab7/'))
		self.assertIn('Status', self.selenium.page_source)

	def test_css_jumbotron_bgcolor(self):
		self.selenium.get('%s%s' %(self.live_server_url, '/Lab7/'))
		jumbotron = self.selenium.find_element_by_class_name('jumbotron').value_of_css_property('background-color')
		self.assertEquals('rgba(209, 178, 94, 1)', jumbotron)

	def test_css_jumbotron_align(self):
		self.selenium.get('%s%s' %(self.live_server_url, '/Lab7/'))
		jumbotron = self.selenium.find_element_by_class_name('jumbotron').value_of_css_property('align-content')
		self.assertEquals('normal', jumbotron)

	def test_title(self):
		self.selenium.get('%s%s' %(self.live_server_url, '/Lab7/'))
		self.assertIn('Statusku', self.selenium.title)


class tests(TestCase):
    def test_lab_6_url_is_exist(self):
        response = Client().get('/Lab7/')
        self.assertEqual(response.status_code,200)

    def test_lab_6_using_to_do_list_template(self):
        response = Client().get('/Lab7/')
        self.assertTemplateUsed(response, 'Status.html')

    def test_lab_6_using_index_func(self):
        found = resolve('/Lab7/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_status(self):
            #Creating a new status
            new_status = Status.objects.create(date=timezone.now(),status='Aku mau latihan ngoding deh')

            #Retrieving all available status
            counting_all_available_status = Status.objects.all().count()
            self.assertEqual(counting_all_available_status,1)

    def test_can_save_a_POST_request(self):
        response = self.client.post('/Lab7/', data={'date': '2017-10-12T14:14', 'status' : 'Maen Dota Kayaknya Enak'})
        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

        self.assertEqual(response.status_code, 200)
       	# self.assertEqual(response['location'], '/Lab6/')

        new_response = self.client.get('/Lab7/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('Hai, Apa Kabar?', html_response)